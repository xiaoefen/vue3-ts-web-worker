# vue3中使用Web Worker多线程（TS风味版）

<br>

JS原味版：[传送门](https://gitee.com/xiaoefen/vue3-web-worker)

<br>

项目详细具体可以看这篇文章：[传送门](https://blog.csdn.net/weixin_42063951/article/details/125300644)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
